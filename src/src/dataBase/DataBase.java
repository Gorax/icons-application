package src.dataBase;

import src.model.IconModel;

import javax.ejb.Stateless;
import java.util.LinkedList;
import java.util.List;

@Stateless
public class DataBase {

    private List<IconModel> iconModelList = new LinkedList<>();

    public List<IconModel> getIconModelList() {
        return iconModelList;
    }
}
