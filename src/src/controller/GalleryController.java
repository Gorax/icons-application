package src.controller;

import src.service.GalleryService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/gallery")
public class GalleryController extends HttpServlet {

    @EJB
    private GalleryService galleryService;

    private int index = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        delete(req);
        if (!download(req, resp)) {
            index = galleryService.changeMainIcon(req);
            req.setAttribute("iconList", galleryService.loadGallery());
            req.setAttribute("index", index);
            req.getRequestDispatcher("gallery.jsp").forward(req, resp);
        }
    }

    private void delete(HttpServletRequest req) {
        if (req.getParameter("delete") != null) {
            galleryService.delete(index);
        }
    }

    private boolean download(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if (req.getParameter("download") != null) {
            if (galleryService.download(index, resp))
                return true;
        }
        return false;
    }
}
