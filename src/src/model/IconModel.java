package src.model;

import java.io.File;

public class IconModel {

    private File file;
    private String icon;

    public IconModel(File file, String icon) {
        this.file = file;
        this.icon = icon;
    }

    public File getFile() {
        return file;
    }

    public String getIcon() {
        return icon;
    }
}
