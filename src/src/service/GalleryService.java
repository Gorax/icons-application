package src.service;

import src.dataBase.DataBase;
import src.model.IconModel;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class GalleryService {

    @EJB
    private DataBase dataBase;

    private List<String> iconList = new ArrayList<>();

    private final int BUFFER_SIZE = 1024 * 100;

    public List<String> loadGallery() {
        iconList.clear();
        for (IconModel element : dataBase.getIconModelList()) {
            iconList.add(element.getIcon());
        }
        return iconList;
    }

    public int changeMainIcon(HttpServletRequest req) {
        for (int i = 0; i < dataBase.getIconModelList().size(); i++) {
            if (req.getParameter("" + i) != null) {
                return i;
            }
        }
        return 0;
    }

    public void delete(int index) {
        if (!dataBase.getIconModelList().isEmpty())
            dataBase.getIconModelList().remove(index);
    }

    public boolean download(int index, HttpServletResponse response) throws IOException {
        OutputStream outStream = null;
        FileInputStream inputStream = null;

        if (!dataBase.getIconModelList().isEmpty()) {
            File file = dataBase.getIconModelList().get(index).getFile();

            if (file.exists()) {

                String mimeType = "application/octet-stream";
                response.setContentType(mimeType);

                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", file.getName());
                response.setHeader(headerKey, headerValue);

                try {

                    outStream = response.getOutputStream();
                    inputStream = new FileInputStream(file);
                    byte[] buffer = new byte[BUFFER_SIZE];
                    int bytesRead = -1;

                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outStream.write(buffer, 0, bytesRead);
                    }
                } catch (IOException ioExObj) {
                    ioExObj.printStackTrace();
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }

                    outStream.flush();
                    if (outStream != null) {
                        outStream.close();
                    }
                    return true;
                }
            }
        }
        return false;
    }
}
