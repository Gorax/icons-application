package src.service;

import src.dataBase.DataBase;
import src.model.IconModel;
import src.service.mapper.IconMapper;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.File;

@Stateless
public class HomeService {

    @EJB
    private DataBase dataBase;

    @EJB
    private IconMapper iconMapper;

    public boolean loadFile(File file) {
        if (validate(file)) {
            String image = iconMapper.fileToString(file);
            if (image == null)
                return false;

            dataBase.getIconModelList().add(new IconModel(file, image));
            return true;
        } else {
            return false;
        }
    }

    private boolean validate(File file) {
        if (file.getName().endsWith(".jpg") && file != null) {
            return true;
        }
        return false;
    }
}
