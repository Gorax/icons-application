<%@ page import="java.util.List" %>
<%@ page import="jdk.nashorn.internal.objects.NativeUint8Array" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Icon application</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">

</head>

<body>
<%
    List<String> imageList = (List<String>) request.getAttribute("iconList");
    Integer mainIcon = (Integer) request.getAttribute("index");
%>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand active" href="">Icon application</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="gallery">Graphic gallery</a>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="" id="navbarDropdownPortfolio" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Icon options
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                        <a class="dropdown-item" href="gallery?delete">Delete</a>
                        <a class="dropdown-item" href="gallery?download">Download</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
    <h1 class="my-4 text-center text-lg-left">Gallery</h1>
    <center>

        <div class="col-lg-9">
            <div class="card-body">
                <%
                    if (!imageList.isEmpty() && imageList != null){
                %>
                <img class="card-img-top img-fluid" src="<%=imageList.get(mainIcon)%>" alt="Something went wrong ;D">
                <%
                    }
                %>
            </div>
        </div>

        <div class="row text-center text-lg-left">

            <%
                for (int i = 0; i < imageList.size(); i++) {
            %>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <a href="gallery?<%=i%>" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="<%=imageList.get(i)%>" alt="something went wrong ;D">
                </a>
            </div>
            <%
                }
            %>
        </div>
    </center>
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
